(function() {
	'use strict';

	function MaklermandatSendController($state, $localStorage) {
        
    console.log('MaklermandatSendController');
    
    this.client = $localStorage.client;
    
    var dataURL =  $localStorage.client['signature']
    
    document.getElementById('signature-preview').src = dataURL;

    this.send = function(){
      $state.go('app.maklermandat-confirm');  
    };
      
	}

	angular
		.module('masa.maklermandat')
		.controller('MaklermandatSendController', MaklermandatSendController);

})();
