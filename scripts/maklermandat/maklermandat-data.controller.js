(function() {
  'use strict';


  function MaklermandatDataController( $localStorage, $state, $ionicScrollDelegate) {
      
    console.log( 'MaklermandatDataController' );
  
    var vm = this;

  
    vm.client = $localStorage.client || {}; 
      
    this.onFormSubmit = function(){
    
      if(vm.form.$valid){        
      
        $localStorage.client['valid'] = true;
        $state.go('app.maklermandat-signature');  

      } else {
        $ionicScrollDelegate.resize();  
      }          
    };

  }
  
  angular.module('masa.maklermandat').controller( 
    'MaklermandatDataController', MaklermandatDataController );

})();
