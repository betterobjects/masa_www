(function() {
	'use strict';

	angular
		.module('masa.maklermandat', [
			'ionic',
			'ngCordova',
      'ngMessages',
      'ngStorage'
		])
    .config(function($stateProvider) {
			$stateProvider
				.state('app.maklermandat-data', {
					url: '/maklermandat/data',
					views: {
						'menuContent': {
							templateUrl: 'scripts/maklermandat/view/data.html' ,
							controller: 'MaklermandatDataController as vm'
						}
					}
				})
        .state('app.maklermandat-signature', {
          url: '/maklermandat/signature',
          views: {
            'menuContent': {
              templateUrl: 'scripts/maklermandat/view/signature.html',
              controller: 'MaklermandatSignatureController as vm'
            }
          }
        })
        .state('app.maklermandat-send', {
          url: '/maklermandat/send',
          views: {
            'menuContent': {
              templateUrl: 'scripts/maklermandat/view/send.html',
              controller: 'MaklermandatSendController as vm'
            }
          }
        })
        .state('app.maklermandat-confirm', {
          url: '/maklermandat/confirm',
          views: {
            'menuContent': {
              templateUrl: 'scripts/maklermandat/view/confirm.html',
              controller: 'MaklermandatConfirmController as vm'
            }
          }
        });
		});
})();
