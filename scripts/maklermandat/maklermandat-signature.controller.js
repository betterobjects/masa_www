(function() {
	'use strict';


  function MaklermandatSignatureController(menuItems, $localStorage, $window, $state ) {
        
    console.log('MaklermandatSignatureController');
        
    var vm = this;

    this.init = function(){
      var canvas = document.querySelector("canvas");
      vm.signaturePad = new SignaturePad(canvas);
      
      var signature = $localStorage.client ? $localStorage.client['signature'] : null;
      if( signature ) vm.signaturePad.fromDataURL(signature);
    };
  
    this.setSignature = function(){
      var dataUrl = vm.signaturePad.toDataURL(); 
      if( $localStorage.client == null ) $localStorage.client = {};

      $localStorage.client['signature'] = dataUrl;
      $state.go('app.maklermandat-send');
    };
        
    angular.element($window).bind('resize', function(){
      vm.calculateDimensions();       
    });
    
    this.calculateDimensions = function(gesture) {
      this.dev_width = $window.innerWidth;
      this.dev_height = $window.innerHeight;
    };    
    
    this.calculateDimensions();
    this.init();
	}

	angular
		.module('masa.maklermandat')
		.controller('MaklermandatSignatureController', MaklermandatSignatureController);

})();
