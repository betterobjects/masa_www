(function() {
	'use strict';

	angular
		.module('masa.tarif-haftpflicht', ['ionic', 'ngCordova'] )
		.config(function($stateProvider) {
      
			$stateProvider.state( 'app.tarif-haftpflicht-data', {
				url: '/tarif/haftpflicht/data/:tarifId',
				views: {
					'menuContent': {
						templateUrl: 'scripts/tarif-haftpflicht/view/data.html',
						controller: 'TarifHaftpflichtController as vm'
					}
				}
			});
        
		  $stateProvider.state( 'app.tarif-haftpflicht-premium', {
				url: '/tarif/haftpflicht/premium/:tarifId/:gruppeId',
				views: {
					'menuContent': {
						templateUrl: 'scripts/tarif-haftpflicht/view/premium.html',
						controller: 'TarifHaftpflichtController as vm'
					}
				}
			});
      
		});

})();
