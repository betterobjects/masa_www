(function() {
	'use strict';

  var tarifHaftpflichtData = {      
    version: '201601121122261452597746',
    basis : {
      name: 'BASIS', 
      items: {
        single: {
          name: 'Single',
          premium: 39.99
        },
        familie: {
          name: 'Familien',
          premium: 61.36
        },
        senior : {
          name: 'Senioren (Single)',
          premium: 36.26
        }
      }
    }, 
    top : {
      name: 'TOP', 
      items: {
        single: {
          name: 'Single',
          premium: 89.44
        },
        familie: {
          name: 'Familien',
          premium: 101.00
        },
        senior : {
          name: 'Senioren (Single)',
          premium: 81.81
        }
      }
    }
  };
  
  angular.module('masa.tarif-haftpflicht').value( 'tarifHaftpflichtData', tarifHaftpflichtData );

})();
