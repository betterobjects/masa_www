(function() {
	'use strict';

	function TarifHaftpflichtController($localStorage, $location, $state, $stateParams, $ionicPopup, tarifHaftpflichtData) {
    console.log('TarifHaftpflichtController');
    
    var vm = this;
    
    var tarife = tarifHaftpflichtData;
            
    if( $stateParams.tarifId ) {
      vm.tarifId = $stateParams.tarifId;
      vm.tarif = tarife[vm.tarifId];      

      vm.gruppeId = $stateParams.gruppeId;    
      vm.gruppe = vm.tarif['items'][vm.gruppeId];
    }    
    
    /**
     * 
     */
    vm.onCalculate = function(){
  
      $state.go('app.tarif-haftpflicht-premium', { tarifId: vm.tarifId, gruppeId: vm.gruppeId } );
    };
    
    /**
     * 
     */
    vm.onApplication = function(){
  
      $localStorage.calculation = { 
        tarif: {
          type: 'Haftplicht', 
          name: vm.tarif.name + ' ' + vm.gruppe.name,          
        },
        premium: vm.gruppe.premium,
        attrs: {
          Typ: vm.gruppe.name,
          foo: 'bar'
        }
      };
  
      $state.go( 'app.insurance-application-data' );
    };       
	}

	angular.module('masa.tarif-haftpflicht').controller(
    'TarifHaftpflichtController', TarifHaftpflichtController);

})();
