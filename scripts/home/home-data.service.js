(function() {
	'use strict';

	function homeDataService() {
		return {
			phoneNumber: '+49 821 71008 500',
			email: 'info@manaug.de',
			officeLocation: '48.3650244,10.925700600000027',
			facebookPage: 'https://www.facebook.com/'
		};
	}

	angular.module('supermodular.home').factory('homeDataService', homeDataService);

})();
