(function() {
	'use strict';
  
	angular
		.module('supermodular.home', ['ionic','ngCordova','supermodular.common','masa.shared'])

    .factory('resetLocalStorage', function($localStorage){
      return function(){
        $localStorage.client = {};
      };
    })

		.config(function($stateProvider) {
			$stateProvider
				.state('app.home', {
					url: '/home',
					views: {
						'menuContent': {
							templateUrl: 'scripts/home/home.html',
							controller: 'HomeController as vm'
						}
					},
          onEnter: function( resetLocalStorage ){
            resetLocalStorage();
          }
				});
		});
})();
