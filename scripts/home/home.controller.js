(function() {
	'use strict';

	function HomeController($state, AppInfo) {

    var vm = this;
    
    vm.agentGiven = AppInfo.agentGiven;
    
    vm.onSettings = function(){
      $state.go('app.settings');
    };
    
    vm.onTarifauswahl = function(){
      if( ! AppInfo.agentGiven() ) return;
      $state.go('app.tarifauswahl');
    };

    vm.onMandat = function(){
      if( ! AppInfo.agentGiven() ) return;
      $state.go('app.maklermandat-data');
    };


    function sendQueue(){
      console.log('sendQueue start');
      
      console.log('sendQueue end');
    }
    
    console.log('before call sendQueue');
    setTimeout( sendQueue, 0 );
    console.log('after call sendQueue');

	}
  
  
  angular
	  .module('supermodular.home')
    .controller('HomeController', HomeController);
  
})();
