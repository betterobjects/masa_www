(function() {
	'use strict';

	angular
		.module('masa.insurance-application', ['ionic','ngCordova', 'masa.shared'])
  
		.config(function($stateProvider) {
			
      $stateProvider.state('app.insurance-application-data', {
				url: '/insurance-application-data',
				views: {
					'menuContent': {
						templateUrl: 'scripts/insurance-application/view/data.html',
						controller: 'InsuranceApplicationDataController as vm'
					}
				}
			});
      
      $stateProvider.state('app.insurance-application-signature', {
				url: '/insurance-application-signature',
				views: {
					'menuContent': {
						templateUrl: 'scripts/insurance-application/view/signature.html',
						controller: 'InsuranceApplicationSignatureController as vm'
					}
				}
			});

      $stateProvider.state('app.insurance-application-send', {
				url: '/insurance-application-send',
				views: {
					'menuContent': {
						templateUrl: 'scripts/insurance-application/view/send.html',
						controller: 'InsuranceApplicationSendController as vm'
					}
				}
			});

      $stateProvider.state('app.insurance-application-confirm', {
				url: '/insurance-application-confirm',
				views: {
					'menuContent': {
						templateUrl: 'scripts/insurance-application/view/confirm.html',
						controller: 'InsuranceApplicationConfirmController as vm'
					}
				}
			});
      
		});
    
})();
