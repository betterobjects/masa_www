(function() {
	'use strict';


  function InsuranceApplicationSendController($state, $localStorage, $ionicPopup, $ionicHistory, $resource, Antrag, AppInfo) {

    var vm = this;
    
    vm.send_failed = false;
    
    vm.calculation = $localStorage.calculation;
    vm.client = $localStorage.client;
    
    var dataURL =  $localStorage.client['signature']    
    document.getElementById('signature-preview').src = dataURL;
    
    /**
    *
    */
    var send = function(){
      
      var antragData = {
        antrag: {
          'app_version'           : AppInfo.version,

          'client_firstname'      : $localStorage.client.firstname,
          'client_lastname'       : $localStorage.client.lastname,
          'client_address1'       : $localStorage.client.adress1, 
          'client_address2'       : $localStorage.client.adress2, 
          'client_zip'            : $localStorage.client.zip,
          'client_city'           : $localStorage.client.city,
          'client_phone'          : $localStorage.client.phone,
          'client_email'          : $localStorage.client.email,
            
          'vermittler_firstname'  : $localStorage.agent.firstname,
          'vermittler_lastname'   : $localStorage.agent.lastname,
          'vermittler_email'      : $localStorage.agent.email, 
          'vermittler_nr'         : $localStorage.agent.id, 
          
          'tarif_version'         : vm.calculation.tarif.version, 
          'tarif_typ'             : vm.calculation.tarif.type,      // beware not tpye!
          'tarif_name'            : vm.calculation.tarif.name, 
          'premium'               : vm.calculation.premium * 100.0, // integer in rails
          'attrs'                 : vm.calculation.attrs,          
          
          'signature'             : vm.client.signature
        }        
      };
      
      var antrag = new Antrag( antragData );
      
      antrag.$save( 
        function(){
          $ionicHistory.nextViewOptions({ disableBack: true });
          $state.go('app.insurance-application-confirm');           
        },
        function(){
          if( ! $localStorage.send_queue ) $localStorage.send_queue = [];
          $localStorage.send_queue.push( antragData );
          vm.send_failed = true;
        }
      );
    };
    
    /*
    *
    */
    vm.onSend = function(){

      vm.send_failed = false;
      
      var confirmPopup = $ionicPopup.confirm({
        title: 'Antrag senden',
        template: 'Soll der Antrag so versendet werden?'
      });

      confirmPopup.then(function( ok ) {
        if( ok ) send();
      });
    };
    
    /**
     *
    */
    vm.onHome = function(){
      $state.go('app.home');
    };

  }

	angular
		.module('masa.insurance-application')
		.controller('InsuranceApplicationSendController', InsuranceApplicationSendController);

})();
