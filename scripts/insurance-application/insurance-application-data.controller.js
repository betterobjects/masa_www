(function() {
	'use strict';


  function InsuranceApplicationDataController($state, $localStorage) {
    
    var vm = this;
    
    vm.calculation = $localStorage.calculation;

    if( $localStorage.client == null || $localStorage.client.firstname == null ) $localStorage.client = {
      anrede: 'Herr',
      firstname: 'Thomas',
      lastname: 'Rekittke-Möller',
      address1: 'Barmbeker Str. 171',
      zip: 22299,
      city: 'Hamburg',
      phone: '015114965228',
      email: 't.rekittke@impeo.de',
      iban: '123456789012345678'
    };
        
    vm.client = $localStorage.client || {};
        
    /**
     *
     */
    vm.onSubmit = function(){
      
      if(! vm.applicationForm.$valid) return;
        
      $state.go('app.insurance-application-signature' );
    };

  }

	angular
		.module('masa.insurance-application')
		.controller('InsuranceApplicationDataController', InsuranceApplicationDataController);

})();
