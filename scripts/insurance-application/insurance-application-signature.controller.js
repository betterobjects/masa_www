(function() {
	'use strict';


  function InsuranceApplicationSignatureController($state, $localStorage, $window) {
        
    var vm = this;

    vm.init = function(){
      var canvas = document.getElementById("application-signature");
      vm.signaturePad = new SignaturePad(canvas);
      
      var signature = $localStorage.client ? $localStorage.client['signature'] : null;
      if( signature ) vm.signaturePad.fromDataURL(signature);
    };
  
    vm.onSignature = function(){

      var canvas = document.getElementById("application-signature");
      
      if( isCanvasBlank( canvas ) ){
        alert('Bitte Unterschrift erfassen');
        return;
      }
      
      var dataUrl = vm.signaturePad.toDataURL(); 
      if( $localStorage.client == null ) $localStorage.client = {};

      $localStorage.client['signature'] = dataUrl;      
      
      $state.go('app.insurance-application-send');
    };
        
    angular.element($window).bind('resize', function(){
      vm.calculateDimensions();       
    });
    
    vm.calculateDimensions = function(gesture) {
      this.dev_width = $window.innerWidth;
      this.dev_height = $window.innerHeight;
    };    
    
    vm.onClear = function(){
      var canvas = document.getElementById("application-signature");
      var context = canvas.getContext('2d');
      context.clearRect(0, 0, canvas.width, canvas.height);      
    };

    vm.calculateDimensions();
    vm.init();    

  }
  
  var isCanvasBlank = function(canvas) {
      var blank = document.createElement('canvas');
      blank.width = canvas.width;
      blank.height = canvas.height;

      return canvas.toDataURL() == blank.toDataURL();
  }

	angular
		.module('masa.insurance-application')
		.controller('InsuranceApplicationSignatureController', InsuranceApplicationSignatureController);

})();
