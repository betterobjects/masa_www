(function() {
	'use strict';


  function InsuranceApplicationConfirmController( $localStorage ) {
    
    var vm = this;
    
    vm.calculation = $localStorage.calculation;
  }

	angular
		.module('masa.insurance-application')
		.controller('InsuranceApplicationConfirmController', InsuranceApplicationConfirmController);

})();
