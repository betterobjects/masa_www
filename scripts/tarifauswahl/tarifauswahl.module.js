(function() {
	'use strict';

	angular
		.module('masa.tarifauswahl', ['ionic','ngCordova'])
  
		.config(function($stateProvider) {
	
  		$stateProvider
				.state('app.tarifauswahl', {
					url: '/tarifauswahl',
					views: {
						'menuContent': {
							templateUrl: 'scripts/tarifauswahl/view/tarifauswahl.html',
							controller: 'TarifauswahlController as vm'
						}
					}
				});
        
  		$stateProvider
				.state('app.tarifauswahl-not', {
					url: '/tarifauswahl-not',
					views: {
						'menuContent': {
							templateUrl: 'scripts/tarifauswahl/view/not.html',
							controller: 'TarifauswahlController as vm'
						}
					}
				});
        
		});
})();
