(function() {
	'use strict';


  function TarifauswahlController($localStorage) {
    $localStorage.calculation = null;
    if( $localStorage.client == null ) $localStorage.client = {};
    if( $localStorage.client.infos == null ) $localStorage.client.infos = {};
  };

	angular
		.module('masa.tarifauswahl')
		.controller('TarifauswahlController', TarifauswahlController);

})();
