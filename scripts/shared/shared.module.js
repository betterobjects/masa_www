(function() {
	'use strict';

  angular
  	.module('masa.shared', ['ionic','ngCordova'])

    .factory('Antrag', function($resource) {
      return $resource('http://localhost:3000/api/v1/antrag');
    })
    
    .factory('Mandat', function($resource) {
      return $resource('http://localhost:3000/api/v1/mandat');
    })
    
    .factory('AppInfo', function( $localStorage){
      return {
        version: '201601131001551452679315',
        
        agentGiven: function(){
          if( ! $localStorage.agent ) return false;
          if( ! $localStorage.agent.email ) return false;
          if( ! $localStorage.agent.id ) return false;
          return true;
        }
        
      }
    })
  ;
    
})();
