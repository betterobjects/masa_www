(function() {
	'use strict';

	angular
		.module('masa.tarifauswahl', [
			'ionic',
			'ngCordova'
		])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.tarifauswahl', {
					url: '/tarifauswahl',
					views: {
						'menuContent': {
							templateUrl: 'scripts/tarifauswahl/tarifauswahl.html',
							controller: 'TarifauswahlController as vm'
						}
					}
				});
		});
})();
