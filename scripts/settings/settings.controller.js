(function() {
	'use strict';


  function SettingsController($localStorage, $state, AppInfo) {
    
    var vm = this;
    
    if( ! $localStorage.agent ) $localStorage.agent = {};
    
    vm.agent = $localStorage.agent;
    
    vm.agentGiven = AppInfo.agentGiven;
    
    /**
     *
    */
    vm.onOk = function(){
      
      $state.go('app.home');        
    };

    /**
     *
    */
    vm.onDelete = function(){
      
      $localStorage.agent = {};
      $state.go('app.home');        
    };
    
  }

	angular
		.module('masa.settings')
		.controller('SettingsController', SettingsController);		

})();
