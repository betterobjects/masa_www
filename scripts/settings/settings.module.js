(function() {
	'use strict';

	angular
		.module('masa.settings', ['ionic','ngCordova', 'masa.shared'])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.settings', {
					url: '/settings',
					views: {
						'menuContent': {
							templateUrl: 'scripts/settings/view/settings.html',
							controller: 'SettingsController as vm'
						}
					}
				});
		});
})();
