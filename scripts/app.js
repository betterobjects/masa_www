// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('masa', [
	'ionic',
	'ionic.service.core',
	'config',
	'ionic.service.core',
	'ionic.service.push',
  'common.fancy-select',
  'masa.shared',
  'masa.tarifauswahl',
  'masa.tarif-haftpflicht',
  'masa.tarif-glas',
  'masa.insurance-application',
  'masa.maklermandat',
  'masa.settings',
	'supermodular.common',
	'supermodular.home',
	'supermodular.map',
	'supermodular.wordpress',
	'supermodular.menu',
	'supermodular.elements',
	'supermodular.popover-menu',
	'gMaps',
	'ngCordova', 
  'ngStorage',
  'ngResource'
])

.value('_', window._)

.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)

		if (window.cordova && window.cordova.plugins.Keyboard) {
			window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})

.config(function($urlRouterProvider, $compileProvider, $localStorageProvider, $ionicConfigProvider) {
	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile|content):|data:image\//);
	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/home');
  $localStorageProvider.setKeyPrefix('masa.0.1.');
  $ionicConfigProvider.views.maxCache(0); // disable view cache
  $ionicConfigProvider.tabs.position('bottom'); // https://forum.ionicframework.com/t/footer-issue-on-android/40087
})


.directive('includeReplace', function() {
  return {
    require: 'ngInclude',
    restrict: 'A',
    link: function(scope, el, attrs) {
      el.html(el.children().html()); // My changes in the line
    }
  };
})

;
