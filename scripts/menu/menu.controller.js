(function() {
	'use strict';

	angular
		.module('supermodular.menu')
		.controller('MenuController', MenuController);

	MenuController.$inject = ['$ionicSideMenuDelegate', '$ionicHistory'];

	/* @ngInject */
	function MenuController($ionicSideMenuDelegate, $ionicHistory) {

    /** was a nice attempt, but no **
    
    this.close = function(){
      $ionicSideMenuDelegate.toggleRight();

      $ionicHistory.nextViewOptions({
        disableAnimate: true
      });
    }; 
    */
	}
})();
