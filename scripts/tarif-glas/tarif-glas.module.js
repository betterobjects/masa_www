(function() {
	'use strict';

	angular
		.module('masa.tarif-glas', ['ionic', 'ngCordova'] )
		.config(function($stateProvider) {
      
			$stateProvider.state( 'app.tarif-glas-data', {
				url: '/tarif/glas/data/:tarifId',
				views: {
					'menuContent': {
						templateUrl: 'scripts/tarif-glas/view/data.html',
						controller: 'TarifGlasController as vm'
					}
				}
			});
        
		  $stateProvider.state( 'app.tarif-glas-premium', {
				url: '/tarif/glas/premium',
				views: {
					'menuContent': {
						templateUrl: 'scripts/tarif-glas/view/premium.html',
						controller: 'TarifGlasController as vm'
					}
				}
			});
      
		});

})();
