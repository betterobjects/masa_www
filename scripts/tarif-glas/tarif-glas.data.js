(function() {
	'use strict';

  var tarifGlasData = {
    version: '201601121122001452597720',
    basis : {
      name: 'BASIS', 
      perqm: 0.124    
    }, 
    top : {
      name: 'TOP', 
      perqm: 0.185
    }
  };
  
  angular.module('masa.tarif-glas').value( 'tarifGlasData', tarifGlasData );

})();
