(function() {
	'use strict';

	function TarifGlasController($localStorage, $state, $stateParams, $filter, tarifGlasData) {
    
    var vm = this;
    
    var tarife = tarifGlasData;
    
    if( $stateParams.tarifId ) {
      vm.tarifId = $stateParams.tarifId;
      vm.tarif = tarife[vm.tarifId];
    }    
    
    vm.calculation = $localStorage.calculation || { 
      tarif: {
        type: 'Haushaltsglas', 
        name: vm.tarif.name,          
      },
      premium: null,
      qm: $localStorage.client.infos['glas-qm']
    };
    
    /**
     * 
     */
    vm.onCalculate = function(){

      if( ! vm.calculation.qm ) return;
      
      $localStorage.client.infos['glas-qm'] = vm.calculation.qm; 
      
      vm.calculation.premium = vm.tarif.perqm * vm.calculation.qm;      
      
      $localStorage.calculation = vm.calculation;
      
      $state.go('app.tarif-glas-premium' );
    };
    
    /**
     * 
     */
    vm.onApplication = function(){
      
      $localStorage.calculation.attrs = {
        'qm Glasfläche' : $filter('number')( vm.calculation.qm, 0 )
      };
      
      $state.go( 'app.insurance-application-data' );
    };       
	}

	angular.module('masa.tarif-glas').controller(
    'TarifGlasController', TarifGlasController);

})();
